
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {

  numero: number = 0;
  mensaje: string = '';
  sumar(x: number){
    this.mensaje = '';
    if (x<50) {
      this.numero++;
    } else {
      this.mensaje = 'El limite es 50';
    }
  }

  restar(x: number){
    this.mensaje = '';
    if (x>0) {
      this.numero--;
    } else {
      this.mensaje = 'El limite es 0'; 
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
